/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at May 8, 2019 2:26:53 PM
 * ----------------------------------------------------------------
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.data;

import de.hybris.platform.cmsfacades.data.ComposedTypeData;

public  class PageTypeData extends ComposedTypeData 
{

 

	/** <i>Generated property</i> for <code>PageTypeData.type</code> property defined at extension <code>cmsfacades</code>. */
		
	private String type;
	
	public PageTypeData()
	{
		// default constructor
	}
	
		
	
	public void setType(final String type)
	{
		this.type = type;
	}

		
	
	public String getType() 
	{
		return type;
	}
	


}
